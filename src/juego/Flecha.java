package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Flecha {
	private double x;
	private double y;
	private Image img;

	public Flecha(double x, double y, double size) {
		this.x = x;
		this.y = y;
		this.img = Herramientas.cargarImagen("misc/flecha-celeste.png");
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(img, x, y, 0, 0.4);
	}

}
