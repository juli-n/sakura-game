package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Kunoichi {
	private double x;
	private double y;
	private double size;
	private Image sakuraMirandoAAbajo;
	private Image sakuraMirandoAArriba;
	private Image sakuraMirandoALaIzquierda;
	private Image sakuraMirandoALaDerecha;

	private Image inoMirandoALaIzquierda;
	private Image inoMirandoALaDerecha;
	private Image inoMirandoAAbajo;
	private Image inoMirandoAArriba;

	private double speed;
	private String nombre;
	private Direccion direccion;

	public Kunoichi(String nombre, double x, double y, double size, double speed) {
		this.nombre = nombre;
		this.x = x;
		this.y = y;
		this.size = size;
		this.speed = speed;
		this.direccion = new Direccion();
		this.sakuraMirandoAAbajo = Herramientas.cargarImagen("misc/sakura_abajo.png");
		this.sakuraMirandoAArriba = Herramientas.cargarImagen("misc/sakura_arriba.png");
		this.sakuraMirandoALaIzquierda = Herramientas.cargarImagen("misc/sakura_izquierda.png");
		this.sakuraMirandoALaDerecha = Herramientas.cargarImagen("misc/sakura_derecha.png");
		this.inoMirandoAAbajo = Herramientas.cargarImagen("misc/ino_abajo.png");
		this.inoMirandoAArriba = Herramientas.cargarImagen("misc/ino_arriba.png");
		this.inoMirandoALaIzquierda = Herramientas.cargarImagen("misc/ino_izquierda.png");
		this.inoMirandoALaDerecha = Herramientas.cargarImagen("misc/ino_derecha.png");
	}

	public void dibujar(Entorno entorno) {
		if (nombre.equalsIgnoreCase("Sakura")) {
			if (direccion.isLeft()) {
				entorno.dibujarImagen(this.sakuraMirandoALaIzquierda, x, y, 0, 1.2);
			}
			if (direccion.isRight()) {
				entorno.dibujarImagen(this.sakuraMirandoALaDerecha, x, y, 0, 1.2);
			}
			if (direccion.isUp()) {
				entorno.dibujarImagen(this.sakuraMirandoAArriba, x, y, 0, 1.2);
			}
			if (direccion.isDown()) {
				entorno.dibujarImagen(this.sakuraMirandoAAbajo, x, y, 0, 1.2);
			}

		} else if (nombre.equalsIgnoreCase("Ino")) {
			if (direccion.isLeft()) {
				entorno.dibujarImagen(this.inoMirandoALaIzquierda, x, y, 0, 1.3);
			}
			if (direccion.isRight()) {
				entorno.dibujarImagen(this.inoMirandoALaDerecha, x, y, 0, 1.3);
			}
			if (direccion.isUp()) {
				entorno.dibujarImagen(this.inoMirandoAArriba, x, y, 0, 1.3);
			}
			if (direccion.isDown()) {
				entorno.dibujarImagen(this.inoMirandoAAbajo, x, y, 0, 1.3);
			}
		}
	}

	public void mover() {
		x += direccion.sentidoHorizontal() * speed;
		y += direccion.sentidoVertical() * speed;
	}

	// movimientos
	public void turnLeft() {
		direccion.turnLeft();
	}

	public void turnRight() {
		direccion.turnRight();
	}

	public void turnUp() {
		direccion.turnUp();
	}

	public void turnDown() {
		direccion.turnDown();
	}

	public boolean isLeft() {
		return direccion.isLeft();
	}

	public boolean isRight() {
		return direccion.isRight();
	}

	public boolean isUp() {
		return direccion.isUp();
	}

	public boolean isDown() {
		return direccion.isDown();
	}

	// colision con los bordes
	public boolean chocasteArriba(Entorno entorno) {
		return y < size / 2;
	}

	public boolean chocasteAbajo(Entorno entorno) {
		return y > entorno.alto() - size / 2;
	}

	public boolean chocasteIzquierda(Entorno entorno) {
		return x < size / 2;
	}

	public boolean chocasteDerecha(Entorno entorno) {
		return x > entorno.ancho() - size / 2;
	}

	public boolean chocasteConManzana(Manzana m) {
		return (m.getX() - m.getAncho() / 2 < x + size / 2) && (m.getX() + m.getAncho() / 2 > x - size / 2)
				&& (m.getY() - m.getAlto() / 2 < y + size / 2) && (y - size / 2 < m.getY() + m.getAlto() / 2);
	}

	public boolean chocasteConNinja(Ninja n) {
		return (n.getX() - n.getSize() / 2 < x + size / 4) && (x - size / 4 < n.getX() + n.getSize() / 2)
				&& (n.getY() - n.getSize() / 2 < y + size / 2) && (y - size / 2 < n.getY() + n.getSize() / 2);
	}

	public boolean entregoPedido(Casa c) {
		return (c.getX() - c.getSize() < x + size) && (x - size < c.getX() + c.getSize())
				&& (c.getY() - c.getSize() < y + size) && (y - size < c.getY() + c.getSize());
	}

	public Rasengan disparar() {
		Rasengan r = new Rasengan(1, this.x + this.size / 40, this.y - this.size / 20, new Direccion(direccion));
		return r;
	}

	public Rasengan dispararJutsu() {
		Rasengan j = new Rasengan(2, this.x + this.size / 40, this.y - this.size / 20, new Direccion(direccion));
		return j;
	}

}
