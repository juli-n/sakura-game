package juego;

import java.awt.Color;
import java.util.Random;

import entorno.Entorno;

public class Casa {
	private double x;
	private double y;
	private double size;

	public Casa(double x, double y, double size) {
		this.x = x;
		this.y = y;
		this.size = size;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarRectangulo(x, y, 45, 45, 0, Color.white);
	}

	public static int elegirCasa(Casa[] casas) {
		Random r = new Random();
		int casa = r.nextInt(casas.length);
		return casa;
	}

	public Flecha marcarCasaConUnaFlecha() {
		Flecha f = new Flecha(this.x, this.y - this.size * 3, this.size);
		return f;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getSize() {
		return size;
	}

}
