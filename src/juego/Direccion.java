package juego;

public class Direccion {

	public final static int LEFT = 1; // -1
	public final static int RIGHT = 2; // +1
	public final static int UP = 3; // -1
	public final static int DOWN = 4; // +1
	private int dir;

	public Direccion() {
		this.dir = DOWN;
	}

	public Direccion(int dir) {
		this.dir = dir;
	}
	
	public Direccion(Direccion direccion) {
		this.dir = direccion.dir;
	}

	public boolean isLeft() {
		return dir == LEFT;
	}

	public boolean isRight() {
		return dir == RIGHT;
	}

	public boolean isUp() {
		return dir == UP;
	}

	public boolean isDown() {
		return dir == DOWN;
	}

	public void turnLeft() {
		dir = LEFT;
	}

	public void turnRight() {
		dir = RIGHT;
	}

	public void turnUp() {
		dir = UP;
	}

	public void turnDown() {
		dir = DOWN;
	}

	public double sentidoHorizontal() {
		return dir == LEFT ? -1 : dir == RIGHT ? 1 : 0;
	}

	public double sentidoVertical() {
		return dir == UP ? -1 : dir == DOWN ? 1 : 0;
	}

}
