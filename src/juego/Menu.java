package juego;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Menu extends InterfaceJuego {

	private Entorno entorno;
	private Image img;
	public static int cantidadJugadores;

	public Menu() {
		this.entorno = new Entorno(this, "Sakura Ikebana Delivery", 800, 600);
		this.img = Herramientas.cargarImagen("misc/menu.png");
		entorno.iniciar();
	}

	public void dibujarMenu(Entorno entorno) {
		entorno.dibujarImagen(img, entorno.ancho() / 2, entorno.alto() / 2, 0);
		entorno.cambiarFont("", 20, Color.decode("#BD64BD"));
		entorno.escribirTexto("Si desea jugar en modo 2 jugadores", entorno.ancho() / 4, entorno.alto() - 50);
		entorno.escribirTexto("Presione '2'", entorno.ancho() / 2.5, entorno.alto() - 20);
	}

	public void tick() {
		dibujarMenu(entorno);

		if (entorno.sePresiono('2')) {
			cantidadJugadores = 2;
			Juego juego = new Juego();
			entorno.setDefaultCloseOperation(Entorno.EXIT_ON_CLOSE);
			entorno.removeNotify();
			entorno.removeAll();
		}

		if (entorno.sePresiono(entorno.TECLA_ENTER)) {
			Juego juego = new Juego();
			entorno.setDefaultCloseOperation(Entorno.EXIT_ON_CLOSE);
			entorno.removeNotify();
			entorno.removeAll();
		}

		if (entorno.sePresiono('q')) {
			entorno.removeNotify();
			entorno.removeAll();
		}
	}

}
