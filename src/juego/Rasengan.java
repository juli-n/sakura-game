package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Rasengan {

	private double x;
	private double y;
	private double size;
	private int tipo;
	private Image rasengan;
	private Image jutsuIzquierda;
	private Image jutsuDerecha;
	private Image jutsuArriba;
	private Image jutsuAbajo;
	private double speed;
	private Direccion direccion;

	public Rasengan(int tipo, double x, double y, Direccion direccion) {
		this.tipo = tipo;
		this.x = x;
		this.y = y;
		this.rasengan = Herramientas.cargarImagen("misc/rasengan.png");
		this.jutsuIzquierda = Herramientas.cargarImagen("misc/jutsu_izquierda.png");
		this.jutsuDerecha = Herramientas.cargarImagen("misc/jutsu_derecha.png");
		this.jutsuArriba = Herramientas.cargarImagen("misc/jutsu_arriba.png");
		this.jutsuAbajo = Herramientas.cargarImagen("misc/jutsu_abajo.png");
		this.speed = 3;
		this.size = 30;
		this.direccion = direccion;
	}

	public void dibujar(Entorno entorno) {
		if (tipo == 1) {
			entorno.dibujarImagen(rasengan, x, y, 0, 0.1);
		} else if (tipo == 2) {
			if (direccion.isLeft()) {
				entorno.dibujarImagen(this.jutsuIzquierda, x, y, 0, 1);
			}
			if (direccion.isRight()) {
				entorno.dibujarImagen(this.jutsuDerecha, x, y, 0, 1);
			}
			if (direccion.isUp()) {
				entorno.dibujarImagen(this.jutsuArriba, x, y, 0, 1);
			}
			if (direccion.isDown()) {
				entorno.dibujarImagen(this.jutsuAbajo, x, y, 0, 1);
			}
		}

	}

	public void mover(Entorno entorno) {
		if (x > 0 || x < entorno.ancho()) {
			x += direccion.sentidoHorizontal() * speed;
		}

		if (y > 0 || y < entorno.alto()) {
			y += direccion.sentidoVertical() * speed;
		}
	}

	public boolean tocasteAlNinja(Ninja n) {
		return (n.getX() - n.getSize() / 2 < x + size / 2) && (x - size / 2 < n.getX() + n.getSize() / 2)
				&& (n.getY() - n.getSize() / 2 < y + size / 2) && (y - size / 2 < n.getY() + n.getSize() / 2);
	}

	public boolean chocasteConBorde(Entorno entorno) {
		return x < size / 2 || x > entorno.ancho() - size / 2 || y < size / 2 || y > entorno.alto() - size / 2;
	}

	public boolean chocasteConManzana(Manzana m) {
		return (m.getX() - m.getAncho() / 2 < x + size / 2) && (m.getX() + m.getAncho() / 2 > x - size / 2)
				&& (m.getY() - m.getAlto() / 2 < y + size / 2) && (y - size / 2 < m.getY() + m.getAlto() / 2);
	}

}
