package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Ninja {

	private double x;
	private double y;
	private Image img;
	private double speed;
	private double size;
	private Direccion direccion;

	public Ninja(double x, double y, double size, double speed, Direccion direccion) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.img = Herramientas.cargarImagen("misc/ninja.png");
		this.speed = speed;
		this.direccion = direccion;
	}

	public void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.img, x, y, 0, 0.8);
	}

	// movimientos
	public void mover(Entorno e) {
		x += direccion.sentidoHorizontal() * speed;
		if (x > e.ancho()) {
			x = 0;
		} else if (x < 1) {
			x = e.ancho();
		}

		y += direccion.sentidoVertical() * speed;
		if (y < 1) {
			y = e.alto();
		} else if (y > e.alto()) {
			y = 0;
		}
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getSize() {
		return size;
	}

}
