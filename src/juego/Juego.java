package juego;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;
	private Kunoichi sakura;
	private Kunoichi ino;
	private Rasengan rasengan;
	private Rasengan jutsu;
	private Flecha flecha;

	private Ninja[] ninjas;
	private Manzana[] manzanas;
	private Casa[] casas;
	private Casa casaSeleccionada;

	private boolean perdio;
	private Image gameOver;

	private boolean gano;
	private Image youWin;

	private Image mapa;

	private int ninjasMuertos;
	private int puntosSakura;
	private int puntosIno;

	public Juego() {

		// Inicializa los objetos
		entorno = new Entorno(this, "Sakura Ikebana Delivery", 800, 600);
		sakura = new Kunoichi("Sakura", entorno.ancho() / 2, entorno.alto() / 2, 48, 2);

		if (Menu.cantidadJugadores == 2) {
			ino = new Kunoichi("Ino", entorno.ancho() / 2, entorno.alto() / 3, 48, 2);
		}

		double ancho = 140;
		double alto = 140;

		manzanas = new Manzana[12];

		manzanas[0] = new Manzana(100, 70, ancho, alto);
		manzanas[1] = new Manzana(300, 70, ancho, alto);
		manzanas[2] = new Manzana(500, 70, ancho, alto);
		manzanas[3] = new Manzana(700, 70, ancho, alto);

		manzanas[4] = new Manzana(100, 280, ancho, alto);
		manzanas[5] = new Manzana(300, 280, ancho, alto);
		manzanas[6] = new Manzana(500, 280, ancho, alto);
		manzanas[7] = new Manzana(700, 280, ancho, alto);

		manzanas[8] = new Manzana(100, 480, ancho, alto);
		manzanas[9] = new Manzana(300, 480, ancho, alto);
		manzanas[10] = new Manzana(500, 480, ancho, alto);
		manzanas[11] = new Manzana(700, 480, ancho, alto);

		// inicializo las casas
		casas = new Casa[36];
		casas[0] = new Casa(70, 120, 10);
		casas[1] = new Casa(150, 120, 10);
		casas[2] = new Casa(150, 50, 10);

		casas[3] = new Casa(270, 120, 10);
		casas[4] = new Casa(350, 120, 10);
		casas[5] = new Casa(350, 50, 10);

		casas[6] = new Casa(470, 120, 10);
		casas[7] = new Casa(550, 120, 10);
		casas[8] = new Casa(550, 50, 10);

		casas[9] = new Casa(650, 120, 10);
		casas[10] = new Casa(760, 120, 10);
		casas[11] = new Casa(650, 50, 10);

		casas[12] = new Casa(70, 330, 10);
		casas[13] = new Casa(150, 330, 10);
		casas[14] = new Casa(130, 230, 10);

		casas[15] = new Casa(260, 330, 10);
		casas[16] = new Casa(300, 230, 10);
		casas[17] = new Casa(340, 330, 10);

		casas[18] = new Casa(470, 230, 10);
		casas[19] = new Casa(550, 230, 10);
		casas[20] = new Casa(550, 330, 10);

		casas[21] = new Casa(650, 230, 10);
		casas[22] = new Casa(760, 330, 10);
		casas[23] = new Casa(650, 330, 10);

		casas[24] = new Casa(70, 430, 10);
		casas[25] = new Casa(150, 530, 10);
		casas[26] = new Casa(150, 430, 10);

		casas[27] = new Casa(270, 530, 10);
		casas[28] = new Casa(350, 430, 10);
		casas[29] = new Casa(350, 530, 10);

		casas[30] = new Casa(470, 430, 10);
		casas[31] = new Casa(550, 430, 10);
		casas[32] = new Casa(500, 530, 10);

		casas[33] = new Casa(650, 430, 10);
		casas[34] = new Casa(760, 430, 10);
		casas[35] = new Casa(650, 530, 10);

		// inicializo los ninjas
		ninjas = new Ninja[5];

		ninjas[0] = new Ninja(entorno.ancho(), entorno.alto() - 220, 50, 1, new Direccion(Direccion.LEFT));
		ninjas[1] = new Ninja(entorno.ancho() / 30, entorno.alto() - 420, 50, 1, new Direccion(Direccion.RIGHT));
		ninjas[2] = new Ninja(entorno.ancho() / 4, entorno.alto() - 550, 50, 1, new Direccion(Direccion.UP));
		ninjas[3] = new Ninja(entorno.ancho() - 200, entorno.alto() - 100, 50, 1, new Direccion(Direccion.UP));
		ninjas[4] = new Ninja(entorno.ancho() / 2, entorno.alto(), 50, 1, new Direccion(Direccion.DOWN));

		casaSeleccionada = null;
		flecha = null;
		rasengan = null;
		jutsu = null;

		youWin = Herramientas.cargarImagen("misc/you_win.png");
		gameOver = Herramientas.cargarImagen("misc/game_over.png");
		mapa = Herramientas.cargarImagen("misc/mapa.png");
		puntosSakura = 0;
		puntosIno = 0;

		entorno.iniciar();
	}

	public void tick() {

		// Si quiere volver a jugar
		if ((perdio || gano) && entorno.sePresiono(entorno.TECLA_ENTER)) {
			Juego juego = new Juego();
			entorno.removeAll();
			entorno.removeNotify();
		}

		if ((perdio || gano) && entorno.sePresiono('q')) {
			entorno.removeAll();
			entorno.removeNotify();
		}

		// si perdio o gano
		if (perdio) {
			entorno.dibujarImagen(gameOver, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
			entorno.cambiarFont("", 20, Color.decode("#BD64BD"));
			entorno.escribirTexto("Si  desea  volver  a  jugar  presione  ENTER", entorno.ancho() / 4,
					entorno.alto() - 530);
			entorno.escribirTexto("Si  desea  salir  presione  Q", entorno.ancho() / 3, entorno.alto() - 500);
			dibujarCantidadNinjasEliminados();
			dibujarPuntaje();
			return;
		}

		if (gano) {
			entorno.dibujarImagen(youWin, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);
			entorno.cambiarFont("", 20, Color.decode("#BD64BD"));
			entorno.escribirTexto("Si  desea  volver  a  jugar  presione  ENTER", entorno.ancho() / 4,
					entorno.alto() - 530);
			entorno.escribirTexto("Si  desea  salir  presione  Q", entorno.ancho() / 3, entorno.alto() - 500);
			dibujarCantidadNinjasEliminados();
			dibujarPuntaje();
			return;
		}

		if (puntosSakura == 100 || (ino != null && puntosIno == 100)) {
			gano = true;
		}

		entorno.dibujarImagen(mapa, entorno.ancho() / 2, entorno.alto() / 2, 0, 1);

		if (casaSeleccionada == null) {
			casaSeleccionada = casas[Casa.elegirCasa(casas)];
			if (flecha == null) {
				flecha = casaSeleccionada.marcarCasaConUnaFlecha();
			}
		}

		if (sakura.entregoPedido(casaSeleccionada)) {
			puntosSakura += 5;
			casaSeleccionada = null;
			flecha = null;
		}

		if (ino != null && casaSeleccionada != null && ino.entregoPedido(casaSeleccionada)) {
			puntosIno += 5;
			casaSeleccionada = null;
			flecha = null;
		}

		if (flecha != null) {
			flecha.dibujar(entorno);
		}

		dibujarCantidadNinjasEliminados();
		dibujarPuntaje();

		for (Ninja n : ninjas) {
			if (n != null) {
				n.dibujar(entorno);
				n.mover(entorno);
			}
		}

		// si algun ninja fue eliminado lo creo nuevamente
		for (int i = 0; i < ninjas.length; i++) {
			if (ninjas[i] == null) {
				if (i == 0) {
					ninjas[i] = new Ninja(entorno.ancho(), entorno.alto() - 220, 50, 1, new Direccion(Direccion.LEFT));
				}
				if (i == 1) {
					ninjas[i] = new Ninja(entorno.ancho() / 30, entorno.alto() - 420, 50, 1,
							new Direccion(Direccion.RIGHT));
				}
				if (i == 2) {
					ninjas[i] = new Ninja(entorno.ancho() / 4, entorno.alto() - 550, 50, 1,
							new Direccion(Direccion.UP));
				}
				if (i == 3) {
					ninjas[i] = new Ninja(entorno.ancho() - 200, entorno.alto() - 100, 50, 1,
							new Direccion(Direccion.UP));
				}
				if (i == 4) {
					ninjas[i] = new Ninja(entorno.ancho() / 2, entorno.alto() - 50, 50, 1,
							new Direccion(Direccion.DOWN));
				}
			}
		}

		if (rasengan != null) {
			rasengan.dibujar(entorno);
			rasengan.mover(entorno);
		}

		if (ino != null && jutsu != null) {
			jutsu.dibujar(entorno);
			jutsu.mover(entorno);
		}

		if (ino != null) {
			ino.dibujar(entorno);
		}

		sakura.dibujar(entorno);

		// movimientos de sakura
		for (Manzana m : manzanas) {
			if (sakura.chocasteConManzana(m) && sakura.isLeft()) {
				sakura.turnRight();
				sakura.mover();
			} else if (sakura.chocasteConManzana(m) && sakura.isRight()) {
				sakura.turnLeft();
				sakura.mover();
			} else if (sakura.chocasteConManzana(m) && sakura.isUp()) {
				sakura.turnDown();
				sakura.mover();
			} else if (sakura.chocasteConManzana(m) && sakura.isDown()) {
				sakura.turnUp();
				sakura.mover();
			}
		}

		if (!sakura.chocasteIzquierda(entorno) && entorno.estaPresionada('a') && !entorno.estaPresionada('w')
				&& !entorno.estaPresionada('s') && !entorno.estaPresionada('d')) {
			sakura.turnLeft();
			sakura.mover();
		} else if (!sakura.chocasteDerecha(entorno) && entorno.estaPresionada('d') && !entorno.estaPresionada('w')
				&& !entorno.estaPresionada('s') && !entorno.estaPresionada('a')) {
			sakura.turnRight();
			sakura.mover();
		} else if (!sakura.chocasteArriba(entorno) && entorno.estaPresionada('w') && !entorno.estaPresionada('a')
				&& !entorno.estaPresionada('d') && !entorno.estaPresionada('s')) {
			sakura.turnUp();
			sakura.mover();
		} else if (!sakura.chocasteAbajo(entorno) && entorno.estaPresionada('s') && !entorno.estaPresionada('a')
				&& !entorno.estaPresionada('d') && !entorno.estaPresionada('w')) {
			sakura.turnDown();
			sakura.mover();
		}

		// movimientos de ino
		if (ino != null) {
			for (Manzana m : manzanas) {
				if (ino.chocasteConManzana(m) && ino.isLeft()) {
					ino.turnRight();
					ino.mover();
				} else if (ino.chocasteConManzana(m) && ino.isRight()) {
					ino.turnLeft();
					ino.mover();
				} else if (ino.chocasteConManzana(m) && ino.isUp()) {
					ino.turnDown();
					ino.mover();
				} else if (ino.chocasteConManzana(m) && ino.isDown()) {
					ino.turnUp();
					ino.mover();
				}

			}
		}

		if (ino != null) {
			if (!ino.chocasteIzquierda(entorno) && entorno.estaPresionada(entorno.TECLA_IZQUIERDA)
					&& !entorno.estaPresionada(entorno.TECLA_DERECHA) && !entorno.estaPresionada(entorno.TECLA_ARRIBA)
					&& !entorno.estaPresionada(entorno.TECLA_ABAJO)) {
				ino.turnLeft();
				ino.mover();
			} else if (!ino.chocasteDerecha(entorno) && entorno.estaPresionada(entorno.TECLA_DERECHA)
					&& !entorno.estaPresionada(entorno.TECLA_IZQUIERDA) && !entorno.estaPresionada(entorno.TECLA_ARRIBA)
					&& !entorno.estaPresionada(entorno.TECLA_ABAJO)) {
				ino.turnRight();
				ino.mover();
			} else if (!ino.chocasteArriba(entorno) && entorno.estaPresionada(entorno.TECLA_ARRIBA)
					&& !entorno.estaPresionada(entorno.TECLA_ABAJO) && !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)
					&& !entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				ino.turnUp();
				ino.mover();
			} else if (!ino.chocasteAbajo(entorno) && entorno.estaPresionada(entorno.TECLA_ABAJO)
					&& !entorno.estaPresionada(entorno.TECLA_ARRIBA) && !entorno.estaPresionada(entorno.TECLA_IZQUIERDA)
					&& !entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				ino.turnDown();
				ino.mover();

			}
		}

		// disparo de rasengan
		if (entorno.estaPresionada(entorno.TECLA_ESPACIO) && rasengan == null) {
			rasengan = sakura.disparar();
			Herramientas.play("misc/rasengan.wav");
		}

		if (rasengan != null && rasengan.chocasteConBorde(entorno)) {
			rasengan = null;
		}

		for (Manzana m : manzanas) {
			if (rasengan != null && rasengan.chocasteConManzana(m)) {
				rasengan = null;
			}
		}

		for (int i = 0; i < ninjas.length; i++) {
			if (rasengan != null && ninjas[i] != null && rasengan.tocasteAlNinja(ninjas[i])) {
				ninjas[i] = null;
				rasengan = null;
				ninjasMuertos++;
			}
		}

		// disparo de jutsu
		if (ino != null && entorno.estaPresionada('m') && jutsu == null) {
			jutsu = ino.dispararJutsu();
		}

		if (jutsu != null && jutsu.chocasteConBorde(entorno)) {
			jutsu = null;
		}

		for (Manzana m : manzanas) {
			if (jutsu != null && jutsu.chocasteConManzana(m)) {
				jutsu = null;
			}
		}

		for (int i = 0; i < ninjas.length; i++) {
			if (jutsu != null && ninjas[i] != null && jutsu.tocasteAlNinja(ninjas[i])) {
				ninjas[i] = null;
				jutsu = null;
				ninjasMuertos++;
			}
		}

		// colision Sakura y/o Ino con ninja
		for (Ninja n : ninjas) {
			if (n != null && (sakura.chocasteConNinja(n) || (ino != null && ino.chocasteConNinja(n)))) {
				perdio = true;
			}
		}
	}

	public void dibujarPuntaje() {
		entorno.cambiarFont("", 20, Color.WHITE);
		entorno.escribirTexto("Puntos de Sakura " + this.puntosSakura, entorno.ancho() / 1.5, 20);
		if (Menu.cantidadJugadores == 2) {
			entorno.escribirTexto("Puntos de Ino " + this.puntosIno, entorno.ancho() / 2.5, 20);
		}
	}

	public void dibujarCantidadNinjasEliminados() {
		entorno.cambiarFont("", 20, Color.WHITE);
		entorno.escribirTexto("Ninjas eliminados  " + this.ninjasMuertos, entorno.ancho() / 22, 20);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
//		Juego juego = new Juego();
		Menu menu = new Menu();
	}

}
